#pragma once

/**
 A point 
*/
class Point
{
private:
    /* data */
public:
    Point(/* args */);
    ~Point();
    double x=0.0, y=0.0;
    /**
    Prints coordinate
    */
    void print();
};
