"""
Conan file for Testing
"""

import conan
from conan.tools.cmake import CMake, CMakeDeps, CMakeToolchain


class Logger(conan.ConanFile):
    """
    Conan recipe for Testing
    """
    settings = ("build_type", )
    requires = ("spdlog/1.10.0", )
    default_options = {"*:fPIC": True, "*:shared": False}

    def generate(self):
        """
        Generates cmake code for dependencies
        """
        cmake_deps = CMakeDeps(self)
        cmake_deps.generate()

        tool_chain = CMakeToolchain(self)
        tool_chain.generate()

    def build(self):
        """
        Builds FluidProps
        """
        cmake = CMake(self)
        cmake.configure()
        cmake.build()
