#!/bin/bash
export CONAN_HOME=`pwd`/.conan
conan install . --build=missing --output-folder=build
conan build . --output-folder build
