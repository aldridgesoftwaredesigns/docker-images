#include "Point.h"
#include "spdlog/spdlog.h"

#include <iostream>

/**
Test program that tests the CPP Docker image
*/
int main(int argc, char **argv)
{
    Point testPoint;
    testPoint.x = 100.0;
    testPoint.y = 200.0;
    testPoint.print();
    spdlog::set_level(spdlog::level::info);
    spdlog::info("Point: x={0}, y={1}", testPoint.x, testPoint.y);

    return 0;
}
